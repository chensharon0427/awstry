package com.example.awstry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwstryApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwstryApplication.class, args);
    }

}
